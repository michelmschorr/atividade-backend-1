const { Router } = require('express');
const UserController = require('../models/UserController');
const ProductController = require('../models/ProductController');

const router = Router();


router.get('/users', UserController.index);
router.get('/users:id', UserController.show);
router.post('/users', UserController.create);
router.put('/users:id', UserController.update);
router.delete('/users:id', UserController.destroy);


router.get('/products', ProductController.index);
router.get('/users:id', ProductController.show);
router.post('/users', ProductController.create);
router.put('/users:id', ProductController.update);
router.delete('/users:id', ProductController.destroy);

module.exports = router;



