const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Product = sequelize.define('Product', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    category: {
        type: DataTypes.STRING,
        allowNull: false
    },
    
    price: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    
    location: {
        type: DataTypes.STRING,
        allowNull: false
    },
    
    inventory: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    
    description: {
        type: DataTypes.STRING,
        
    }
    
    
},
{
    timestamps: false
});




Product.associate = function(models) {
    Product.belongsTo(models.User)
}